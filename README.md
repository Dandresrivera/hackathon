# Clase 1
# Herramientas y Definiciones

Proyecto desarrollado en el marco de la semana TIC alcaldía de ibagué

¿qué usaremos?
# Aprendiendo y comunidad
* [Stackoverflow](https://stackoverflow.com/)

# Programas
* [VSCode](https://code.visualstudio.com/)

* [Git](https://git-scm.com/)

* [Android Studio](https://developer.android.com/studio?gclid=CjwKCAjw7rWKBhAtEiwAJ3CWLLTgsTOCsW4OeX7QNuNf68k8cSv2qE6OsUD-Lg5bVO97WyzCt5tFnhoCu-sQAvD_BwE&gclsrc=aw.ds) 

* [SDK ANDROID](https://www.xatakandroid.com/tutoriales/como-instalar-el-android-sdk-y-para-que-nos-sirve) 

* [Flutter](https://flutter.dev/docs/get-started/install/windows)

* [Mac OSX - XCode solo para Sistema operativo Mac Os](https://itunes.apple.com/hn/app/xcode/id497799835?l=en&mt=12)


# DISEÑO
* [DISEÑOS EN ZEPLIN](https://zpl.io/adQe9Ge)



# Plugins

* [Awesome Flutter Snippets](https://marketplace.visualstudio.com/items?itemName=Nash.awesome-flutter-snippets)

* [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)

* [Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code)

* [Flutter](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter)

* [Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme)

* [Paste JSON as Code](https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype)

* [Terminal](https://marketplace.visualstudio.com/items?itemName=formulahendry.terminal)




# Tema que estoy usando:
* [Monokai Night](https://marketplace.visualstudio.com/items?itemName=fabiospampinato.vscode-monokai-night)

# Presentación

* [presentación](https://docs.google.com/presentation/d/1A4GyVugT3xGc4ty3lSYvJzvuo7WkAoLE1P7dbRpXtTo/edit#slide=id.gc6f80d1ff_0_50)


# Configuración del Bracket Pair Colorizer 2
```
"bracket-pair-colorizer-2.colors": [
    "#fafafa",
    "#9F51B6",
    "#F7C244",
    "#F07850",
    "#9CDD29",
    "#C497D4"
],


