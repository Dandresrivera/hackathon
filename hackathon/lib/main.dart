import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Hackathon'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final title;
  MyHomePage({Key? key,  this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[50],
        centerTitle: true,
        elevation: 0,
        title: Text(
          'Promociones',
          style: TextStyle(
            color: Colors.black,
            fontSize: 26,
            fontWeight: FontWeight.w900,
          ),
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 30.2,
              top: 44.3,
              bottom: 30,
            ),
            child: Text(
              "Categorías destacadas",
              style: const TextStyle(
                color: const Color(0xff1e272e),
                fontWeight: FontWeight.w300,
                fontFamily: "Raleway",
                fontStyle: FontStyle.normal,
                fontSize: 20.0,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(
              left: 30.2,
            ),
            child: Row(
              children: [
                categories('Animales', 'resources/icon/animales.png'),
                SizedBox(width: 20),
                categories('Comida', 'resources/icon/comida.png'),
                SizedBox(width: 20),
                categories('Ejercicio', 'resources/icon/ejercicio.png'),
                SizedBox(width: 20),
                categories('Moda', 'resources/icon/moda.png'),
                SizedBox(width: 20),
                categories('Ocio', 'resources/icon/ocio.png'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 30.2,
              top: 44.3,
              bottom: 30,
            ),
            child: Text(
              "Promociones",
              style: const TextStyle(
                color: const Color(0xff1e272e),
                fontWeight: FontWeight.w300,
                fontFamily: "Raleway",
                fontStyle: FontStyle.normal,
                fontSize: 20.0,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          promociones(
            context,
            'resources/imagenes/collar.jpeg',
            'ANIMALES',
            'Collar para mascotas',
            '20 % de descuento - \$25.000',
            'Obten el mejor collar para que luzca tu mascota.',
          ),
          SizedBox(
            height: 10,
          ),
          promociones(
            context,
            'resources/imagenes/MARCUERNAS.jpeg',
            'EJERCICIO',
            'Mancuernas',
            '2*1 - \$30.000',
            '¡Compralas ahora!',
          ),
          SizedBox(
            height: 10,
          ),
          promociones(
            context,
            'resources/imagenes/hamburguesa.png',
            'COMIDA',
            'Chef Burguer',
            'Combo Philadelphia - \$23.000',
            'Combo Philadelphia (Burger + papas + gaseosa).',
          ),
          SizedBox(
            height: 10,
          ),
          promociones(
            context,
            'resources/imagenes/blusa.jpg',
            'MODA',
            'Blusa',
            '25 % de descuento - \$22.000',
            'La comodidad en todo lugar.',
          ),
          SizedBox(
            height: 10,
          ),
          promociones(
            context,
            'resources/imagenes/juego.jpeg',
            'OCIO',
            'Call of duty',
            '50 % de descuento - \$100.000',
            'Aventura.',
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}

Widget categories(String title, String image) {
  return Column(
    children: [
      Container(
        width: 60,
        height: 60,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              color: const Color(0x80d7d7d7),
              offset: Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Image.asset(image),
      ),
      SizedBox(height: 10),
      Text(
        title,
        style: const TextStyle(
          color: const Color(0xff999999),
          fontWeight: FontWeight.w400,
          fontFamily: "Raleway",
          fontStyle: FontStyle.normal,
          fontSize: 14.0,
        ),
      ),
    ],
  );
}

Widget promociones(
  BuildContext context,
  String image,
  String category,
  String title,
  String ofert,
  String description,
) {
  return Padding(
    padding: const EdgeInsets.symmetric(
      horizontal: 30.2,
    ),
    child: Container(
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.25,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage(image),
                ),
              ),
            ),
            Flexible(
              child: Container(
                margin: EdgeInsets.only(
                  left: 10,
                  top: 10,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      category,
                      style: const TextStyle(
                        color: const Color(0xffff9900),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Raleway",
                        fontStyle: FontStyle.normal,
                        fontSize: 13.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 5),
                    Text(
                      title,
                      style: const TextStyle(
                        color: const Color(0xff1e272e),
                        fontWeight: FontWeight.w600,
                        fontFamily: "Raleway",
                        fontStyle: FontStyle.normal,
                        fontSize: 22.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 5),
                    Text(
                      ofert,
                      style: const TextStyle(
                        color: const Color(0xff1e272e),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Raleway",
                        fontStyle: FontStyle.normal,
                        fontSize: 18.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(height: 5),
                    Text(
                      description,
                      style: const TextStyle(
                        color: const Color(0xff999999),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Raleway",
                        fontStyle: FontStyle.normal,
                        fontSize: 15.0,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.17,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: const Color(0x80d7d7d7),
              offset: Offset(0, 2),
              blurRadius: 4,
              spreadRadius: 0,
            ),
          ],
        )),
  );
}
